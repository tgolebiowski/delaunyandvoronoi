#pragma once
#include <unordered_map>
#include <Polycode.h>

using namespace Polycode;

namespace VoronoiName
{
	struct Triangle;
	struct VoronoiNode;
	struct Site;

	struct Edge
	{
		Vector2 point1;
		Vector2 point2;

		Triangle* tri1;
		Triangle* tri2;

		Edge(Vector2 point1, Vector2 point2)
		{
			this->point1 = point1;
			this->point2 = point2;
			tri1 = NULL;
			tri2 = NULL;
		};
	};

	struct Triangle
	{
		Edge* edges[3];
	};

	struct DelaunayTriangle
	{
		Vector2 p1;
		Vector2 p2;
		Vector2 p3;

		///index 0 = p1->p2, index 1 = p2->p3, index 2 = p3->p1
		DelaunayTriangle* neighbors[3];

		void* userData;

		Triangle* baseTri;

		VoronoiNode* node;

		Site* sites[3];

		DelaunayTriangle()
		{
			neighbors[0] = NULL;
			neighbors[1] = NULL;
			neighbors[2] = NULL;
			sites[0] = NULL;
			sites[1] = NULL;
			sites[2] = NULL;
			baseTri = NULL;
			node = NULL;
		};
	};

	struct Circle
	{
		Vector2 center;
		float radius;

		Circle()
		{

		};

		Circle(Triangle* tri);
	};

	struct VoronoiNode
	{
		Vector2 location;
		VoronoiNode* neighbors[3];
		float radius;

		DelaunayTriangle* tri;

		VoronoiNode(Vector2 location)
		{
			this->location = location;
			neighbors[0] = NULL;
			neighbors[1] = NULL;
			neighbors[2] = NULL;

			tri = NULL;
		}
	};

	struct Site
	{
		Vector2 position;
		std::vector<VoronoiNode*> verticies;
		std::vector<Site*> neighbors;
		std::vector<DelaunayTriangle*> touchingTris;

		void* userData;

		Site()
		{
			userData = NULL;
		};
	};

	class Voronoi
	{
	public:
		Voronoi(void);
		///only use this if you're inputting points one at a time and recalculating
		///not just inputting, generating, and using the result
		Voronoi(float avgX, float avgY, float maxX, float maxY, float minX, float minY);
		~Voronoi(void);

		bool generateDelaunayTriangulation();
		void generateVoronoiGraph();
		int pointInTri(DelaunayTriangle* tri, Vector2 point);
		void renderDelaunayMesh(Scene* scene, std::vector<SceneLine*>* lines, Vector2 offset = Vector2(0, 0));
		void renderVoronoiDiagram(Scene* scene, std::vector<SceneLine*>* lines, Vector2 offset = Vector2(0, 0));
		DelaunayTriangle* getTriangleHousingPoint(Vector2 point, DelaunayTriangle* startTri = NULL);
		void easyInsert(Vector2 point);
		///Note: does not clear anything to do with Voronoi diagrams or user data
		///while recalculating, especially with the user data, clear that first!
		void recalculateDelaunayInfo();
		void recalculateVoronoiInfo();
		Site* getNearestNeighbor(Vector2 point, DelaunayTriangle* startTri = NULL);
		void floodFill(std::vector<Site*>* floodedSites, float cutOffDistance, Site* startSite);

		DelaunayTriangle* centerMostTriangle;

		std::vector<Vector2>* getPointList() { return &pointList; };
		std::vector<Triangle*>* getTriangleList() { return &triangleList; };
		std::vector<VoronoiNode*>* getVoronoiGraph() { return &voronoiGraph; };
		std::vector<DelaunayTriangle*>* getDelaunayMesh() { return &delaunayGraph; };
		std::vector<Edge*>* getEdgeList() { return &edgeList; };
		std::vector<Site*>* getSiteList() { return &siteList; };

		std::unordered_map<int, Site*> siteMap;

		float avgX;
		float avgY;

	private:

		Vector2 startPoint1;
		Vector2 startPoint2;
		Vector2 startPoint3;

		std::vector<VoronoiNode*> voronoiGraph;
		std::vector<Vector2> pointList;
		std::vector<Triangle*> triangleList;
		std::vector<DelaunayTriangle*> delaunayGraph;
		std::vector<Edge*> edgeList;
		std::vector<Site*> siteList;

		float minX;
		float minY;
		float maxX;
		float maxY;

		bool flip(Triangle* triangle1, Triangle* triangle2, Edge* edgeInCommon);
		void insertPoint(Triangle* parentTri, Vector2 newPoint, Edge** newEdges, Edge** oldEdges);
		bool pointInTri(Triangle* triangle, Vector2 point);
	};
}