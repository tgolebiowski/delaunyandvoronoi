#include "PolycodeView.h"
#include "Polycode.h"
#include "Delaunay_Src/Triangulator.h"
#include "Delaunay_Src/QuasiCrystal.h"

using namespace Polycode;

class PointArrangeApp {
public:
    PointArrangeApp(PolycodeView *view);
    ~PointArrangeApp();
    
    bool Update();

private:
    Core *core;
	Scene* scene;
	QuasiCrystal* crystal;
	Triangulator* triangulator;

	ScenePrimitive* cursor;
	SceneMesh* triMesh;
	std::vector<SceneMesh*> tris;
	std::vector<ScenePrimitive*> sites;
	std::vector<SceneLine*> debugLines;

	SceneLabel* fpsLabel;
	SceneLabel* timeCheck1;
	SceneLabel* timeCheck2;
	bool click;
};