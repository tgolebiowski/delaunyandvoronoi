#include "Voronoi.h"


Circle::Circle(Triangle* triangle)
{
	Vector2 vert1 = triangle->edges[0]->point1;
	Vector2 vert2 = triangle->edges[0]->point2;
	Vector2 vert3;
	
	if (triangle->edges[1]->point1 != vert1 && triangle->edges[1]->point1 != vert2)
		vert3 = triangle->edges[1]->point1;
	else
		vert3 = triangle->edges[1]->point2;
	
	Vector2 p0 = vert1;
	Vector2 p1 = vert2;
	Vector2 p2 = vert3;

	Vector2 midPoint01 = (p0 + p1) / 2.0;
	Vector2 midPoint12 = (p1 + p2) / 2.0;

	Vector2 bisector1 = p0 - p1;
	bisector1 = Vector2(-bisector1.y, bisector1.x);
	Vector2 bisector2 = p2 - p1;
	bisector2 = Vector2(-bisector2.y, bisector2.x);

	struct Crs{
		static float cross(Vector2 v1, Vector2 v2) {
			return (v1.x * v2.y) - (v1.y * v2.x);
		}
	};

	float t = Crs::cross((midPoint01 - midPoint12), bisector1) / Crs::cross(bisector2, bisector1);
	Vector2 inter = bisector2;
	inter.x *= t;
	inter.y *= t;

	center = midPoint12 + inter;
	radius = (center - p0).length();
};

Voronoi::Voronoi(void)
{

}

Voronoi::Voronoi(float avgX, float avgY, float maxX, float maxY, float minX, float minY)
{
	this->avgX = avgX;
	this->avgY = avgY;
	this->maxX = maxX;
	this->maxY = maxY;
	this->minX = minX;
	this->minY = minY;
}

Voronoi::~Voronoi(void)
{
	for(int i = 0; i < edgeList.size(); i++)
	{
		delete edgeList.at(i);
	}
	edgeList.clear();

	for(int i = 0; i < triangleList.size(); i++)
	{
		delete triangleList.at(i);
	}
	triangleList.clear();

	for(int i = 0; i < delaunayGraph.size(); i++)
	{
		delete delaunayGraph.at(i);
	}
	delaunayGraph.clear();

	for(int i = 0; i < voronoiGraph.size(); i++)
	{
		delete voronoiGraph.at(i);
	}
	voronoiGraph.clear();

}

bool Voronoi::generateDelaunayTriangulation()
{
	minX = 10000.0f;
	maxX = 0.0f;
	minY = 10000.0f;
	maxY = 0.0f;

	for(int i = 0; i < pointList.size(); i++)
	{
		minX = std::min((double)pointList.at(i).x, (double)minX);
		maxX = std::max((double)pointList.at(i).x, (double)maxX);
		minY = std::min((double)pointList.at(i).y, (double)minY);
		maxY = std::max((double)pointList.at(i).y, (double)maxY);
	}

	avgX = minX + maxX;
	avgX /= 2.0f;
	avgY = minY + maxY;
	avgY /= 2.0f;

	startPoint1 = Vector2(avgX, avgY + (3.5f * (maxY - avgY)));
	startPoint2 = Vector2((3.5f * (minX - avgX)) + avgX, (3.5f * (minY - avgY)) + avgY);
	startPoint3 = Vector2((3.5f * (maxX - avgX)) + avgX, avgY);

	triangleList.push_back(new Triangle());
	triangleList.back()->edges[0] = new Edge(startPoint1, startPoint2); 
	triangleList.back()->edges[1] = new Edge(startPoint2, startPoint3);
	triangleList.back()->edges[2] = new Edge(startPoint3, startPoint1);
	triangleList.back()->edges[0]->tri1 = triangleList.back();
	triangleList.back()->edges[1]->tri1 = triangleList.back();
	triangleList.back()->edges[2]->tri1 = triangleList.back();
	edgeList.push_back(triangleList.back()->edges[0]);
	edgeList.push_back(triangleList.back()->edges[1]);
	edgeList.push_back(triangleList.back()->edges[2]);

	for(int i = 0; i < pointList.size(); i++)
	{
		Triangle* tri = NULL;
		int index = -1;
		for(int j = 0; j < triangleList.size(); j++)
		{
			if(pointInTri(triangleList.at(j), pointList.at(i)))
			{
				tri = triangleList.at(j);
				index = j;
				break;
			}
		}

		if(tri != NULL)
		{
			Edge* newEdges[3];
			Edge* oldEdges[3];
			insertPoint(tri, pointList.at(i), newEdges, oldEdges);
			for (int i = 0; i < 3; i++)
			{
				bool flipreturn = flip(newEdges[i]->tri1, newEdges[i]->tri2, newEdges[i]);
			}
			for (int i = 0; i < 3; i++)
			{
				bool flipReturn = flip(oldEdges[i]->tri1, oldEdges[i]->tri2, oldEdges[i]);
			}
			   
		}
	}

	std::vector<Triangle*> edgeTriangles;
	std::vector<Edge*> edgesToRemove;

	for(int i = 0; i < triangleList.size(); i++)
	{
		Vector2 vert1 = triangleList.at(i)->edges[0]->point1;
		Vector2 vert2 = triangleList.at(i)->edges[0]->point2;
		Vector2 vert3;

		if(triangleList.at(i)->edges[1]->point1 != vert1 && triangleList.at(i)->edges[1]->point1 != vert2) vert3 = triangleList.at(i)->edges[1]->point1;
		else vert3 = triangleList.at(i)->edges[1]->point2;

		if((vert1 == startPoint1 || vert1 == startPoint2 || vert1 == startPoint3) ||
			(vert2 == startPoint1 || vert2 == startPoint2 || vert2 == startPoint3) ||
			(vert3 == startPoint1 || vert3 == startPoint2 || vert3 == startPoint3))
		{
			Triangle* tri = triangleList.at(i);
			edgeTriangles.push_back(tri);

			triangleList.erase(triangleList.begin() + i);
			i--;
		}
	}

	for(int i = 0; i < edgeTriangles.size(); i++)
	{
		for(int j = 0; j < 3; j++)
		{
			if(edgeTriangles.at(i)->edges[j] != NULL)
			{
				Edge* edge = edgeTriangles.at(i)->edges[j];

				if((edge->point1 == startPoint1 || edge->point1 == startPoint2 || edge->point1 == startPoint3) ||
					(edge->point2 == startPoint1 || edge->point2 == startPoint2 || edge->point2 == startPoint3))
				{
					for(int k = 0; k < 3; k++)
					{
						if(edge->tri1 != NULL && edge->tri1->edges[k] == edge) edge->tri1->edges[k] = NULL;
						if(edge->tri2 != NULL && edge->tri2->edges[k] == edge) edge->tri2->edges[k] = NULL;
					}
					edgesToRemove.push_back(edge);
					delete edge;
				}
				else
				{
					if(edge->tri1 == edgeTriangles.at(i)) edge->tri1 = NULL;
					if(edge->tri2 == edgeTriangles.at(i)) edge->tri2 = NULL;
				}
			}
		}
		delete edgeTriangles.at(i);
	}

	for(int i = 0; i < edgesToRemove.size(); i++)
	{
		for(int j = 0; j < edgeList.size(); j++)
		{
			if(edgeList.at(j) == edgesToRemove.at(i))
			{
				edgeList.erase(edgeList.begin() + j);
				break;
			}
		}
	}

	float shortestDistance = 10000.0f;

	for(int i = 0; i < triangleList.size(); i++)
	{
		Triangle* tri = triangleList.at(i);
		Vector2 v1 = tri->edges[0]->point1;
		Vector2 v2 = tri->edges[0]->point2;
		Vector2 v3;
		if(tri->edges[1]->point1 != v1 && tri->edges[1]->point1 != v2) v3 = tri->edges[1]->point1;
		else v3 = tri->edges[1]->point2;

		Vector2 test1 = v2 - v1;
		Vector2 test2 = v3 - v1;
		float crossTest = test1.crossProduct(test2);

		DelaunayTriangle* newTri = new DelaunayTriangle();
		newTri->baseTri = tri;

		if(crossTest < 0)
		{
			newTri->p1 = v2;
			newTri->p2 = v1;
			newTri->p3 = v3;
		}
		else
		{
			newTri->p1 = v3;
			newTri->p2 = v1;
			newTri->p3 = v2;
		}

		Vector2 vectorToCenter =  Circle(tri).center - Vector2(0,0);
		if(vectorToCenter.length() < shortestDistance)
		{
			shortestDistance = vectorToCenter.length();
			centerMostTriangle = newTri;
		}

		delaunayGraph.push_back(newTri);
	}

	for(int i = 0; i < triangleList.size(); i++)
	{
		Triangle* tri1 = triangleList.at(i);

		for(int j = 0; j < 3; j++)
		{
			Triangle* tri2 = NULL;
			if(tri1->edges[j]->tri1 == tri1) 
				tri2 = tri1->edges[j]->tri2;
			else							
				tri2 = tri1->edges[j]->tri1;

			if(tri2 != NULL)
			{
				int index1 = i;
				int index2 = -1;
				for(int k = 0; k < triangleList.size(); k++)
				{
					if(triangleList.at(k) == tri2)
					{
						index2 = k;
						break;
					}
				}

				DelaunayTriangle* dTri1 = delaunayGraph.at(index1);
				DelaunayTriangle* dTri2 = delaunayGraph.at(index2);

				if((dTri1->p1 == tri1->edges[j]->point1 && dTri1->p2 == tri1->edges[j]->point2) 
					|| (dTri1->p1 == tri1->edges[j]->point2 && dTri1->p2 == tri1->edges[j]->point1))
				{
					dTri1->neighbors[0] = dTri2;
				}
				else if((dTri1->p2 == tri1->edges[j]->point1 && dTri1->p3 == tri1->edges[j]->point2)
					|| (dTri1->p2 == tri1->edges[j]->point2 && dTri1->p3 == tri1->edges[j]->point1))
				{
					dTri1->neighbors[1] = dTri2;
				}
				else if((dTri1->p3 == tri1->edges[j]->point1 && dTri1->p1 == tri1->edges[j]->point2)
					|| (dTri1->p3 == tri1->edges[j]->point2 && dTri1->p1 == tri1->edges[j]->point1))
				{
					dTri1->neighbors[2] = dTri2;
				}
			}
		}
	}

	return true;
}

void Voronoi::generateVoronoiGraph()
{
	siteMap.clear();
	for(int i = 0; i < triangleList.size(); i++)
	{
		Triangle* tri = triangleList.at(i);
		DelaunayTriangle* delau = delaunayGraph.at(i);
		Circle circum = Circle(tri);

		VoronoiNode* node = new VoronoiNode(circum.center);
		node->radius = circum.radius;
		voronoiGraph.push_back(node);

		node->tri = delaunayGraph.at(i);
		node->tri->node = node;

		if(node->location.x > (avgX + (maxX - avgX) * 1.2)) node->location.x = avgX + (maxX - avgX) * 1.2;
		if(node->location.x < (avgX + (minX - avgX) * 1.2)) node->location.x = avgX + (minX - avgX) * 1.2;
		if(node->location.y > (avgY + (maxY - avgY) * 1.2)) node->location.y = avgY + (maxY - avgY) * 1.2;
		if(node->location.y < (avgY + (minY - avgY) * 1.2)) node->location.y = avgY + (minY - avgY) * 1.2;

		struct SiteKeyGenerator
		{
			static int MakeKey(Vector2 p)
			{
				float x = p.x;
				float y = p.y;

				int siteKey = y;
				int mult = 1;
				while (mult < siteKey)
					mult *= 10;
				siteKey += (siteKey * mult);

				return siteKey;
			};
		};

		int p1Key = SiteKeyGenerator::MakeKey(delau->p1);
		int p2Key = SiteKeyGenerator::MakeKey(delau->p2);
		int p3Key = SiteKeyGenerator::MakeKey(delau->p3);

		Site* site1 = NULL;
		Site* site2 = NULL;
		Site* site3 = NULL;

		std::unordered_map<int, Site*>::iterator find1 = siteMap.find(p1Key);
		if(find1 == siteMap.end())
		{
			siteList.push_back(new Site());
			siteList.back()->position = delau->p1;
			siteList.back()->touchingTris.push_back(delau);
			siteList.back()->verticies.push_back(node);
			std::pair<int, Site*> pair(p1Key, siteList.back());
			siteMap.insert(pair);

			site1 = siteList.back();

			if(delau->p1 == siteList.back()->position) delau->sites[0] = siteList.back();
			else if(delau->p2 == siteList.back()->position) delau->sites[1] = siteList.back();
			else if(delau->p3 == siteList.back()->position) delau->sites[2] = siteList.back();
		}
		else
		{
			find1->second->touchingTris.push_back(delau);
			find1->second->verticies.push_back(node);

			site1 = find1->second;

			if(delau->p1 == find1->second->position) delau->sites[0] = find1->second;
			else if(delau->p2 == find1->second->position) delau->sites[1] = find1->second;
			else if(delau->p3 == find1->second->position) delau->sites[2] = find1->second;
		}

		std::unordered_map<int, Site*>::iterator find2 = siteMap.find(p2Key);
		if(find2 == siteMap.end())
		{
			siteList.push_back(new Site());
			siteList.back()->position = delau->p2;
			siteList.back()->touchingTris.push_back(delau);
			siteList.back()->verticies.push_back(node);
			std::pair<int, Site*> pair(p2Key, siteList.back());
			siteMap.insert(pair);

			site2 = siteList.back();

			if(delau->p1 == siteList.back()->position) delau->sites[0] = siteList.back();
			else if(delau->p2 == siteList.back()->position) delau->sites[1] = siteList.back();
			else if(delau->p3 == siteList.back()->position) delau->sites[2] = siteList.back();
		}
		else
		{
			find2->second->touchingTris.push_back(delau);
			find2->second->verticies.push_back(node);

			site2 = find2->second;

			if(delau->p1 == find2->second->position) delau->sites[0] = find2->second;
			else if(delau->p2 == find2->second->position) delau->sites[1] = find2->second;
			else if(delau->p3 == find2->second->position) delau->sites[2] = find2->second;
		}

		std::unordered_map<int, Site*>::iterator find3 = siteMap.find(p3Key);
		if(find3 == siteMap.end())
		{
			siteList.push_back(new Site());
			siteList.back()->position = delau->p3;
			siteList.back()->touchingTris.push_back(delau);
			siteList.back()->verticies.push_back(node);
			std::pair<int, Site*> pair(p3Key, siteList.back());
			siteMap.insert(pair);

			site3 = siteList.back();

			if(delau->p1 == siteList.back()->position) delau->sites[0] = siteList.back();
			else if(delau->p2 == siteList.back()->position) delau->sites[1] = siteList.back();
			else if(delau->p3 == siteList.back()->position) delau->sites[2] = siteList.back();
		}
		else
		{
			find3->second->touchingTris.push_back(delau);
			find3->second->verticies.push_back(node);

			site3 = find3->second;

			if(delau->p1 == find3->second->position) delau->sites[0] = find3->second;
			else if(delau->p2 == find3->second->position) delau->sites[1] = find3->second;
			else if(delau->p3 == find3->second->position) delau->sites[2] = find3->second;
		}

		bool insert2 = true;
		bool insert3 = true;
		for (int j = 0; j < site1->neighbors.size(); j++)
		{
			if (site1->neighbors[j] == site2) insert2 = false;
			if (site1->neighbors[j] == site3) insert3 = false;
		}
		if (insert2) { site1->neighbors.push_back(site2); site2->neighbors.push_back(site1); }
		if (insert3) { site1->neighbors.push_back(site3); site3->neighbors.push_back(site3); }

		insert3 = true;
		for (int j = 0; j < site2->neighbors.size(); j++)
		{
			if (site2->neighbors[j] == site3) insert3 = false;
		}
		if (insert3) { site2->neighbors.push_back(site3); site3->neighbors.push_back(site2); }
	}

	for(int i = 0; i < voronoiGraph.size(); i++)
	{
		Triangle* relatedTri = triangleList.at(i);
		int neighborIndex = 0;

		for(int j = 0; j < 3; j++)
		{
			if(relatedTri->edges[j]->tri1 != relatedTri && relatedTri->edges[j]->tri1 != NULL)
			{
				int index = -1;

				for(int k = 0; k < triangleList.size(); k++)
				{
					if(triangleList.at(k) == relatedTri->edges[j]->tri1)
					{
						index = k;
						break;
					}
				}

				voronoiGraph.at(i)->neighbors[neighborIndex] = voronoiGraph.at(index);
				neighborIndex++;

			}
			else if(relatedTri->edges[j]->tri2 != relatedTri && relatedTri->edges[j]->tri2 != NULL)
			{
				int index = -1;

				for(int k = 0; k < triangleList.size(); k++)
				{
					if(triangleList.at(k) == relatedTri->edges[j]->tri2)
					{
						index = k;
						break;
					}
				}

				voronoiGraph.at(i)->neighbors[neighborIndex] = voronoiGraph.at(index);
				neighborIndex++;
			}
		}
	}
}

void Voronoi::insertPoint(Triangle* parentTri, Vector2 newPoint, Edge** newEdges, Edge** oldEdges)
{
	Vector2 p0 = parentTri->edges[0]->point1;
	Vector2 p1 = parentTri->edges[0]->point2;
	Vector2 p2;

	oldEdges[0] = parentTri->edges[0];
	oldEdges[1] = parentTri->edges[1];
	oldEdges[2] = parentTri->edges[2];

	if(parentTri->edges[1]->point1 != p0 && parentTri->edges[1]->point1 != p1) p2 = parentTri->edges[1]->point1;
	else if(parentTri->edges[1]->point2 != p0 && parentTri->edges[1]->point2 != p1) p2 = parentTri->edges[1]->point2;

	Edge* newEdge1 = new Edge(newPoint, p0);
	Edge* newEdge2 = new Edge(newPoint, p1);
	Edge* newEdge3 = new Edge(newPoint, p2);
	newEdges[0] = newEdge1;
	newEdges[1] = newEdge2;
	newEdges[2] = newEdge3;

	Triangle* newTri1 = new Triangle();
	Triangle* newTri2 = new Triangle();
	Triangle* newTri3 = new Triangle();

	newTri1->edges[0] = newEdge1;
	newTri1->edges[1] = newEdge2;
	newTri1->edges[2] = parentTri->edges[0];

	if(newTri1->edges[2]->tri1 == parentTri) newTri1->edges[2]->tri1 = newTri1;
	else if(newTri1->edges[2]->tri2 = parentTri) newTri1->edges[2]->tri2 = newTri1;
	else
	{
		int jsfgs = 283;
	}

	newTri2->edges[0] = newEdge2;
	newTri2->edges[1] = newEdge3;

	for(int i = 1; i < 3; i++)
	{
		if((parentTri->edges[i]->point1 == p1 && parentTri->edges[i]->point2 == p2) ||
			(parentTri->edges[i]->point1 == p2 && parentTri->edges[i]->point2 == p1))
		{
			newTri2->edges[2] = parentTri->edges[i];
			break;
		}
	}

	if(newTri2->edges[2]->tri1 == parentTri) newTri2->edges[2]->tri1 = newTri2;
	else if (newTri2->edges[2]->tri2 == parentTri) newTri2->edges[2]->tri2 = newTri2;

	newTri3->edges[0] = newEdge3;
	newTri3->edges[1] = newEdge1;

	for(int i = 1; i < 3; i++)
	{
		if((parentTri->edges[i]->point1 == p0 && parentTri->edges[i]->point2 == p2) ||
			(parentTri->edges[i]->point1 == p2 && parentTri->edges[i]->point2 == p0))
		{
			newTri3->edges[2] = parentTri->edges[i];
			break;
		}
	}

	if(newTri3->edges[2]->tri1 == parentTri) newTri3->edges[2]->tri1 = newTri3;
	else if(newTri3->edges[2]->tri2 == parentTri) newTri3->edges[2]->tri2 = newTri3;

	newEdge1->tri1 = newTri1;
	newEdge1->tri2 = newTri3;
	newEdge2->tri1 = newTri1;
	newEdge2->tri2 = newTri2;
	newEdge3->tri1 = newTri2;
	newEdge3->tri2 = newTri3;

	flip(newTri1->edges[0]->tri1, newTri1->edges[0]->tri2, newTri1->edges[0]);

	for(int i = 0; i < triangleList.size(); i++)
	{
		if(triangleList.at(i) == parentTri)
		{
			delete parentTri;
			triangleList.erase(triangleList.begin() + i);
		}
	}

	triangleList.push_back(newTri1);
	triangleList.push_back(newTri2);
	triangleList.push_back(newTri3);
}

bool Voronoi::pointInTri(Triangle* triangle, Vector2 point)
{
	Vector2 p0 = triangle->edges[0]->point1;
	Vector2 p1 = triangle->edges[0]->point2;
	Vector2 p2;

	if(triangle->edges[1]->point1 != p1 && triangle->edges[1]->point1 != p0) p2 = triangle->edges[1]->point1;
	else p2 = triangle->edges[1]->point2;

	float denominator = ((p1.y - p2.y) * (p0.x - p2.x) + (p2.x -p1.x) * (p0.y - p2.y));

	float a = ((p1.y - p2.y)*(point.x - p2.x) + (p2.x - p1.x)*(point.y - p2.y)) / denominator;
	float b = ((p2.y - p0.y)*(point.x - p2.x) + (p0.x - p2.x)*(point.y - p2.y)) / denominator;
	float c = 1 - a - b;

	return (0 <= a && a <= 1 && 0 <= b && b <= 1 && 0 <= c && c <= 1);
}

bool Voronoi::flip(Triangle* triangle1, Triangle* triangle2, Edge* edgeInCommon)
{
	if(triangle1 == NULL || triangle2 == NULL) return false;
	if(triangle1 == triangle2)
	{
		return false;
	}

	//test that they have edge in common
	bool edgeInTri1 = false;
	bool edgeInTri2 = false;
	for(int i = 0; i < 3; i++)
	{
		if(triangle1->edges[i] == edgeInCommon) edgeInTri1 = true;
		if(triangle2->edges[i] == edgeInCommon) edgeInTri2 = true;
	}

	if(!edgeInTri1 || !edgeInTri2) return false;

	Vector2 tri1OppositeVert;
	Vector2 tri2OppositeVert;

	for(int i = 0; i < 3; i++)
	{
		if(triangle1->edges[i] != edgeInCommon)
		{
			if(triangle1->edges[i]->point1 != edgeInCommon->point1 && triangle1->edges[i]->point1 != edgeInCommon->point2)
			{
				tri1OppositeVert = triangle1->edges[i]->point1;
			}
			else if(triangle1->edges[i]->point2 != edgeInCommon->point1 && triangle1->edges[i]->point2 != edgeInCommon->point2)
			{
				tri1OppositeVert = triangle1->edges[i]->point2;
			}
		}

		if(triangle2->edges[i] != edgeInCommon)
		{
			if(triangle2->edges[i]->point1 != edgeInCommon->point1 && triangle2->edges[i]->point1 != edgeInCommon->point2)
			{
				tri2OppositeVert = triangle2->edges[i]->point1;
			}
			else if(triangle2->edges[i]->point2 != edgeInCommon->point1 && triangle2->edges[i]->point2 != edgeInCommon->point2)
			{
				tri2OppositeVert = triangle2->edges[i]->point2;
			}
		}
	}

	if(tri1OppositeVert == startPoint1 || tri1OppositeVert == startPoint2 || tri1OppositeVert == startPoint3)
	{
		return false;
	}

	if(tri2OppositeVert == startPoint1 || tri2OppositeVert == startPoint2 || tri2OppositeVert == startPoint3)
	{
		return false;
	}

	Circle circleTest1 = Circle(triangle1);
	float distance1 = (circleTest1.center - tri2OppositeVert).length();
	Circle circleTest2 = Circle(triangle2);
	float distance2 = (circleTest2.center - tri1OppositeVert).length();
	bool inCircleTest1 = distance1 <= circleTest1.radius;
	bool inCircleTest2 = distance2 <= circleTest2.radius;

	if(inCircleTest1 && inCircleTest2)
	{
		edgeInCommon->point1 = tri1OppositeVert;
		edgeInCommon->point2 = tri2OppositeVert;

		Edge* tri1EdgeToTrade = NULL;
		int index = -1;
		for(int i = 0; i < 3; i++)
		{
			if(triangle1->edges[i] != edgeInCommon)
			{
				Vector2 vertForTesting;
				if(triangle1->edges[i]->point1 != tri1OppositeVert) vertForTesting = triangle1->edges[i]->point1;
				else if(triangle1->edges[i]->point2 != tri1OppositeVert) vertForTesting = triangle1->edges[i]->point2;

				Vector2 newEdgeCompare = tri2OppositeVert - tri1OppositeVert;
				Vector2 thisEdgeCompare = vertForTesting - tri1OppositeVert;

				float cross = newEdgeCompare.crossProduct(thisEdgeCompare);

				if(cross < 0)
				{
					tri1EdgeToTrade = triangle1->edges[i];
					index = i;
					break;
				}
			}
		}

		Edge* tri2EdgeToTrade = NULL;
		int index2 = -1;
		for(int i = 0; i < 3; i++)
		{
			if(triangle2->edges[i] != edgeInCommon)
			{
				Vector2 vertForTesting;
				if(triangle2->edges[i]->point1 != tri2OppositeVert) vertForTesting = triangle2->edges[i]->point1;
				else if(triangle2->edges[i]->point2 != tri2OppositeVert) vertForTesting = triangle2->edges[i]->point2;

				Vector2 newEdgeCompare = tri1OppositeVert - tri2OppositeVert;
				Vector2 thisEdgeCompare = vertForTesting - tri2OppositeVert;

				float cross = newEdgeCompare.crossProduct(thisEdgeCompare);

				if(cross < 0)
				{
					tri2EdgeToTrade = triangle2->edges[i];
					index2 = i;
					break;
				}
			}
		}

		if(tri1EdgeToTrade->tri1 == triangle1) tri1EdgeToTrade->tri1 = triangle2;
		else if(tri1EdgeToTrade->tri2 == triangle1) tri1EdgeToTrade->tri2 = triangle2;

		if(tri2EdgeToTrade->tri1 == triangle2) tri2EdgeToTrade->tri1 = triangle1;
		else if(tri2EdgeToTrade->tri2 == triangle2) tri2EdgeToTrade->tri2 = triangle1;

		triangle1->edges[index] = tri2EdgeToTrade;
		triangle2->edges[index2] = tri1EdgeToTrade;
		edgeInCommon->point1 = tri1OppositeVert;
		edgeInCommon->point2 = tri2OppositeVert;

		for(int i = 0; i < 3; i++)
		{
			flip(triangle1->edges[i]->tri1, triangle1->edges[i]->tri2, triangle1->edges[i]);
		}

		for(int i = 0; i < 3; i++)
		{
			flip(triangle2->edges[i]->tri1, triangle2->edges[i]->tri2, triangle2->edges[i]);
		}

		return true;
	}
	else
	{
		return false;
	}
}

DelaunayTriangle* Voronoi::getTriangleHousingPoint(Vector2 point, DelaunayTriangle* startTri)
{
	int result = -2;
	DelaunayTriangle* currentTri = startTri;
	if(currentTri == NULL) currentTri = centerMostTriangle;
	do{
		//This is all scoped because it was once a helper method :P
		//
		{
			DelaunayTriangle* tri = currentTri;
			float denominator = ((tri->p2.y - tri->p3.y) * (tri->p1.x - tri->p3.x) + 
				(tri->p3.x -tri->p2.x) * (tri->p1.y - tri->p3.y));

			//a corresponds with edge b/n points 2 & 3
			float a = ((tri->p2.y - tri->p3.y)*(point.x - tri->p3.x) + 
				(tri->p3.x - tri->p2.x)*(point.y - tri->p3.y)) / denominator;
			//b corresponds with edge b/n points 1 & 3
			float b = ((tri->p3.y - tri->p1.y)*(point.x - tri->p3.x) + 
				(tri->p1.x - tri->p3.x)*(point.y - tri->p3.y)) / denominator;
			//c corresponds with edge b/n points 1 & 2
			float c = 1 - a - b;

			bool crossEdgeIndex1 = a < 0;
			bool crossEdgeIndex2 = b < 0;
			bool crossEdgeIndex0 = c < 0;

			if(crossEdgeIndex1 && crossEdgeIndex2)
			{
				result = rand() % 2 + 1;
			}
			else if(crossEdgeIndex1 && crossEdgeIndex0)
			{
				result = rand() % 2;
			}
			else if(crossEdgeIndex0 && crossEdgeIndex2)
			{
				int rando = rand() % 2;

				if(rando == 0) result = 0;
				else if(rando == 1) result = 2;
			}
			else if(crossEdgeIndex0)
			{
				result = 0;
			}
			else if(crossEdgeIndex1)
			{
				result = 1;
			}
			else if(crossEdgeIndex2)
			{
				result = 2;
			}
			else
			{
				result = -1;
			}
		}

		if(result != -1)
		{
			currentTri = currentTri->neighbors[result];
		}
		if(currentTri == NULL)
		{
			result = -1;
		}

	}while(result != -1);

	return currentTri;
}

Site* Voronoi::getNearestNeighbor(Vector2 point, DelaunayTriangle* startTri)
{
	DelaunayTriangle* delau = getTriangleHousingPoint(point, startTri);
	if(delau == NULL) return NULL;

	float shortestDistance = 100000.0f;
	int bestIndex = -1;
	for(int i = 0; i < 3; i++)
	{
		if(delau->sites[i] != NULL)
		{
			float dist = (delau->sites[i]->position - point).length();
			if(shortestDistance > dist) { bestIndex = i; shortestDistance = dist; }
		}
	}

	return delau->sites[bestIndex];
}

void Voronoi::renderDelaunayMesh(Scene* scene, std::vector<SceneLine*>* lines, Vector2 offset)
{
	for(int i = 0; i < delaunayGraph.size(); i++)
	{
		DelaunayTriangle* tri = delaunayGraph.at(i);

		SceneLine* l1 = new SceneLine(Vector3(tri->p1.x - offset.x, tri->p1.y - offset.y, 0.0f),
			Vector3(tri->p2.x - offset.x, tri->p2.y - offset.y, 0.0f));
		SceneLine* l2 = new SceneLine(Vector3(tri->p2.x - offset.x, tri->p2.y - offset.y, 0.0f), 
			Vector3(tri->p3.x - offset.x, tri->p3.y - offset.y, 0.0f));
		SceneLine* l3 = new SceneLine(Vector3(tri->p3.x - offset.x, tri->p3.y - offset.y, 0.0f), 
			Vector3(tri->p1.x - offset.x, tri->p1.y - offset.y, 0.0f));
		scene->addEntity(l1);
		scene->addEntity(l2);
		scene->addEntity(l3);

		lines->push_back(l1);
		lines->push_back(l2);
		lines->push_back(l3);
	}
}

void Voronoi::renderVoronoiDiagram(Scene* scene, std::vector<SceneLine*>* lines, Vector2 offset)
{
	for(int i = 0; i < voronoiGraph.size(); i++)
	{
		Vector2 p = voronoiGraph.at(i)->location;
		for(int j = 0; j < 3; j++)
		{
			if(voronoiGraph.at(i)->neighbors[j] == NULL) continue;

			Vector2 p1 = voronoiGraph.at(i)->neighbors[j]->location;

			SceneLine* l = new SceneLine(Vector3(p.x - offset.x, p.y - offset.y, 0.0f), 
				Vector3(p1.x - offset.x, p1.y - offset.y, 0.0f));
			scene->addEntity(l);
			lines->push_back(l);
		}
	}
}

void Voronoi::floodFill(std::vector<Site*>* floodedSites, float cutOffDistance, Site* startSite)
{
	if (startSite == NULL) return;

	std::vector<Site*> usedSites;
	usedSites.push_back(startSite);

	while (usedSites.size() > 0)
	{
		Site* first = usedSites.front();
		bool siteAdded = false;
		for (int i = 0; i < first->neighbors.size(); i++)
		{
			Site* neighbor = first->neighbors[i];
			bool used = std::find(usedSites.begin(), usedSites.end(), neighbor) != usedSites.end();
			bool exausted = std::find(floodedSites->begin(), floodedSites->end(), neighbor) != floodedSites->end();

			if (!used && !exausted)
			{
				float distanceToSite = (startSite->position - neighbor->position).length();
				if (distanceToSite < cutOffDistance)
				{
					usedSites.push_back(neighbor);
					siteAdded = true;
					break;
				}
			}
		}

		if (!siteAdded)
		{
			usedSites.erase(usedSites.begin());
			floodedSites->push_back(first);
		}
	}
}

void Voronoi::easyInsert(Vector2 point)
{
	Triangle* tri = NULL;
	int index = -1;
	for (int j = 0; j < triangleList.size(); j++)
	{
		if (pointInTri(triangleList.at(j), point))
		{
			tri = triangleList.at(j);
			index = j;
			break;
		}
	}

	if (tri != NULL)
	{
		Edge* newEdges[3];
		Edge* oldEdges[3];
		insertPoint(tri, point, newEdges, oldEdges);
		
		for (int i = 0; i < 3; i++)
		{
			flip(newEdges[i]->tri1, newEdges[i]->tri2, newEdges[i]);
		}
		for (int i = 0; i < 3; i++)
		{
			flip(oldEdges[i]->tri1, oldEdges[i]->tri2, oldEdges[i]);
		}
	}

	Site* newSite = new Site();
	newSite->position = point;
	siteList.push_back(newSite);

	struct SiteKeyGenerator
	{
		static int MakeKey(Vector2 p)
		{
			float x = p.x;
			float y = p.y;

			int siteKey = y;
			int mult = 1;
			while (mult < siteKey)
				mult *= 10;
			siteKey += (siteKey * mult);

			return siteKey;
		};
	};

	int searchKey = SiteKeyGenerator::MakeKey(point);
	std::pair<int, Site*> pair(searchKey, siteList.back());
	siteMap.insert(pair);

	pointList.push_back(point);

}

///Note: does not clear anything to do with Voronoi diagrams or user data
///while recalculating, especially with the user data, clear that first!
void Voronoi::recalculateDelaunayInfo()
{
	for (int i = 0; i < delaunayGraph.size(); i++)
	{
		delete delaunayGraph.at(i);
	}
	delaunayGraph.clear();

	float shortestDistance = -1.0f;

	for(int i = 0; i < triangleList.size(); i++)
	{
		Triangle* tri = triangleList.at(i);
		Vector2 v1 = tri->edges[0]->point1;
		Vector2 v2 = tri->edges[0]->point2;
		Vector2 v3;
		if(tri->edges[1]->point1 != v1 && tri->edges[1]->point1 != v2) v3 = tri->edges[1]->point1;
		else v3 = tri->edges[1]->point2;

		Vector2 test1 = v2 - v1;
		Vector2 test2 = v3 - v1;
		float crossTest = test1.crossProduct(test2);

		DelaunayTriangle* newTri = new DelaunayTriangle();
		newTri->baseTri = tri;

		if(crossTest < 0)
		{
			newTri->p1 = v2;
			newTri->p2 = v1;
			newTri->p3 = v3;
		}
		else
		{
			newTri->p1 = v3;
			newTri->p2 = v1;
			newTri->p3 = v2;
		}

		Vector2 vectorToCenter =  Circle(tri).center - Vector2(0,0);
		if(vectorToCenter.length() < shortestDistance || shortestDistance < 0.0f)
		{
			shortestDistance = vectorToCenter.length();
			centerMostTriangle = newTri;
		}

		delaunayGraph.push_back(newTri);
	}

	for(int i = 0; i < triangleList.size(); i++)
	{
		Triangle* tri1 = triangleList.at(i);

		for(int j = 0; j < 3; j++)
		{
			Triangle* tri2 = NULL;
			if(tri1->edges[j]->tri1 == tri1) tri2 = tri1->edges[j]->tri2;
			else							tri2 = tri1->edges[j]->tri1;

			if(tri2 != NULL)
			{
				int index1 = i;
				int index2 = -1;
				for(int k = 0; k < triangleList.size(); k++)
				{
					if(triangleList.at(k) == tri2)
					{
						index2 = k;
						break;
					}
				}

				DelaunayTriangle* dTri1 = delaunayGraph.at(index1);
				DelaunayTriangle* dTri2 = delaunayGraph.at(index2);

				if((dTri1->p1 == tri1->edges[j]->point1 && dTri1->p2 == tri1->edges[j]->point2) 
					|| (dTri1->p1 == tri1->edges[j]->point2 && dTri1->p2 == tri1->edges[j]->point1))
				{
					dTri1->neighbors[0] = dTri2;
				}
				else if((dTri1->p2 == tri1->edges[j]->point1 && dTri1->p3 == tri1->edges[j]->point2)
					|| (dTri1->p2 == tri1->edges[j]->point2 && dTri1->p3 == tri1->edges[j]->point1))
				{
					dTri1->neighbors[1] = dTri2;
				}
				else if((dTri1->p3 == tri1->edges[j]->point1 && dTri1->p1 == tri1->edges[j]->point2)
					|| (dTri1->p3 == tri1->edges[j]->point2 && dTri1->p1 == tri1->edges[j]->point1))
				{
					dTri1->neighbors[2] = dTri2;
				}
			}
		}
	}
}

void Voronoi::recalculateVoronoiInfo()
{
	for (int i = 0; i < voronoiGraph.size(); i++)
	{
		delete voronoiGraph.at(i);
	}
	voronoiGraph.clear();

	for(int i = 0; i < triangleList.size(); i++)
	{
		Triangle* tri = triangleList.at(i);
		DelaunayTriangle* delau = delaunayGraph.at(i);
		Circle circum = Circle(tri);

		VoronoiNode* node = new VoronoiNode(circum.center);
		node->radius = circum.radius;
		voronoiGraph.push_back(node);

		node->tri = delaunayGraph.at(i);
		node->tri->node = node;

		if(node->location.x > (avgX + (maxX - avgX) * 1.2)) node->location.x = avgX + (maxX - avgX) * 1.2;
		if(node->location.x < (avgX + (minX - avgX) * 1.2)) node->location.x = avgX + (minX - avgX) * 1.2;
		if(node->location.y > (avgY + (maxY - avgY) * 1.2)) node->location.y = avgY + (maxY - avgY) * 1.2;
		if(node->location.y < (avgY + (minY - avgY) * 1.2)) node->location.y = avgY + (minY - avgY) * 1.2;

		struct SiteKeyGenerator
		{
			static int MakeKey(Vector2 p)
			{
				float x = p.x;
				float y = p.y;

				int siteKey = y;
				int mult = 0;
				while (mult < siteKey)
					mult *= 10;
				siteKey += (siteKey * mult);

				return siteKey;
			};
		};

		int p1Key = SiteKeyGenerator::MakeKey(delau->p1);
		int p2Key = SiteKeyGenerator::MakeKey(delau->p2);
		int p3Key = SiteKeyGenerator::MakeKey(delau->p3);

		std::unordered_map<int, Site*>::iterator find1 = siteMap.find(p1Key);
		assert(find1 != siteMap.end());

		find1->second->touchingTris.push_back(delau);
		find1->second->verticies.push_back(node);
		if (delau->p1 == find1->second->position) delau->sites[0] = find1->second;
		else if (delau->p2 == find1->second->position) delau->sites[1] = find1->second;
		else if (delau->p3 == find1->second->position) delau->sites[2] = find1->second;

		std::unordered_map<int, Site*>::iterator find2 = siteMap.find(p2Key);
		assert(find2 != siteMap.end());

		find2->second->touchingTris.push_back(delau);
		find2->second->verticies.push_back(node);
		if (delau->p1 == find2->second->position) delau->sites[0] = find2->second;
		else if (delau->p2 == find2->second->position) delau->sites[1] = find2->second;
		else if (delau->p3 == find2->second->position) delau->sites[2] = find2->second;

		std::unordered_map<int, Site*>::iterator find3 = siteMap.find(p3Key);
		assert(find3 != siteMap.end());

		find3->second->touchingTris.push_back(delau);
		find3->second->verticies.push_back(node);
		if (delau->p1 == find3->second->position) delau->sites[0] = find3->second;
		else if (delau->p2 == find3->second->position) delau->sites[1] = find3->second;
		else if (delau->p3 == find3->second->position) delau->sites[2] = find3->second;

		//does neighbor info of sites
		Site* site1 = find1->second;
		Site* site2 = find2->second;
		Site* site3 = find3->second;
		bool insert2 = true;
		bool insert3 = true;
		for (int j = 0; j < site1->neighbors.size(); j++)
		{
			if (site1->neighbors[j] == site2) insert2 = false;
			if (site1->neighbors[j] == site3) insert3 = false;
		}
		if (insert2) { site1->neighbors.push_back(site2); site2->neighbors.push_back(site1); }
		if (insert3) { site1->neighbors.push_back(site3); site3->neighbors.push_back(site3); }

		insert3 = true;
		for (int j = 0; j < site2->neighbors.size(); j++)
		{
			if (site2->neighbors[j] == site3) insert3 = false;
		}
		if (insert3) { site2->neighbors.push_back(site3); site3->neighbors.push_back(site2); }
	}

	for(int i = 0; i < voronoiGraph.size(); i++)
	{
		Triangle* relatedTri = triangleList.at(i);
		int neighborIndex = 0;

		for(int j = 0; j < 3; j++)
		{
			if(relatedTri->edges[j]->tri1 != relatedTri && relatedTri->edges[j]->tri1 != NULL)
			{
				int index = -1;

				for(int k = 0; k < triangleList.size(); k++)
				{
					if(triangleList.at(k) == relatedTri->edges[j]->tri1)
					{
						index = k;
						break;
					}
				}

				voronoiGraph.at(i)->neighbors[neighborIndex] = voronoiGraph.at(index);
				neighborIndex++;

			}
			else if(relatedTri->edges[j]->tri2 != relatedTri && relatedTri->edges[j]->tri2 != NULL)
			{
				int index = -1;

				for(int k = 0; k < triangleList.size(); k++)
				{
					if(triangleList.at(k) == relatedTri->edges[j]->tri2)
					{
						index = k;
						break;
					}
				}

				voronoiGraph.at(i)->neighbors[neighborIndex] = voronoiGraph.at(index);
				neighborIndex++;
			}
		}
	}
}