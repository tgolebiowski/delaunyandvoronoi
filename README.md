# README #
This repo is broken current version can be found here: https://github.com/tgolebiowski/CompGeo

Implementation of certain Computational Geometery Algorithms, mostly having to do with 2D space partitioning.

Features:
Poisson Disc Point Distribution
Delaunay Triangulation
   -2D search, using triangle walks
   -Flood Fill
   -Incremental point additions available
Voronoi Diagrams